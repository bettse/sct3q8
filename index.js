const util = require('util');
const SerialPort = require('serialport')
const debug = require('debug')('index');

const CRT310 = require('./CRT310');
const CRT310Packet = require('./CRT310Packet');

const NOT_FOUND = -1;

const device = "/dev/ttyUSB0"

// Will store which tracks have data
const tracks = []

function main () {
  const crt310 = new CRT310(device);

  crt310.on('ready', crt310packet => {
    debug('crt310 ready')
    crt310.enable();
  })

  crt310.on('data', packet => {
    debug('incoming', packet)
    if (!packet.positive) {
      if (packet.cm == 0x31) {
        // Ignore status check without card
        return
      }

      crt310.capture();
      console.log('packet not positive, halting commands');
      return
    }

    if (packet.cm == 0x31) {
      const STATUS = {
        NO_CARD: '00',
        GATE: '01',
        CARD: '02',
      }

      switch(packet.statusCode) {
        case STATUS.CARD:
          //console.log('status card');
          crt310.magStatus();
          return;
        case STATUS.GATE:
          //console.log('status gate');
          break;
        case STATUS.NO_CARD:
          //console.log('status no card');
          break;
      }

      setTimeout(() => {
        crt310.statusRequest();
      }, 300)
    } else if (packet.cm == 0x3a) { //enabled
      crt310.statusRequest();
    } else if (packet.cm == 0x34) {
      crt310.magStatus();
    } else if (packet.cm == 0x36 && packet.pm === 0x37) {
      // The 0th byte is '0x60' and i don't know why
      for (var i = 1; i < packet.data.length; i++) {
        if (packet.data[i] === 0x31) {
          tracks.push(i);
        }
      }

      if (tracks.length === 0) {
        crt310.capture()
      } else {
        crt310.readTrack(tracks.shift())
      }
    } else if (packet.cm == 0x36 && packet.pm === 0x31) {
      console.log('track 1', packet.data.toString())
      if (tracks.length > 0) {
        crt310.readTrack(tracks.shift())
      } else {
        crt310.eject()
      }
    } else if (packet.cm == 0x36 && packet.pm === 0x32) {
      console.log('track 2', packet.data.toString())
      if (tracks.length > 0) {
        crt310.readTrack(tracks.shift())
      } else {
        crt310.eject()
      }
    } else if (packet.cm == 0x36 && packet.pm === 0x33) {
      console.log('track 3', packet.data.toString())
      crt310.eject();
    } else if (packet.cm == 0x33 && packet.pm === 0x31) { // capture
      crt310.statusRequest();
    } else if (packet.cm == 0x33 && packet.pm === 0x30) { // Ejected
      crt310.statusRequest();
    }
  })

  console.log("waiting for CRT310");
}

main();
