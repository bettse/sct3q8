const util = require('util');
const SerialPort = require('serialport')
const inquirer = require('inquirer');

const CRT310 = require('./CRT310');
const CRT310Packet = require('./CRT310Packet');

const NOT_FOUND = -1;
const DECIMAL = 10;

const device = "/dev/ttyUSB0"

const crt310 = new CRT310(device);

function askQuestions() {
  inquirer
    .prompt([
      {name: 'room', message: 'Room number', type: 'number', default: 301},
      {name: 'nights', message: 'How many nights', type: 'number', default: 1},
      {name: 'late', message: 'Late checkout', type: 'confirm', default: false},
    ])
    .then((answers) => {
      const { room, nights, late } = answers;
      //{ answers: { room: 301, nights: 1, late: false } }
      // TODO: accomidate people checking in after midnight
      const checkout = new Date()
      checkout.setDate(checkout.getDate() + nights);
      checkout.setHours(10, 00, 00);
      if (late) {
        checkout.setHours(12, 00, 00);
      }
      const time = Math.floor(checkout.getTime()/1000)

      const parts = [
        'ROOM ',
        room.toString(DECIMAL),
        'T',
        time.toString(DECIMAL),
      ]
      const track1 = Buffer.from(parts.join(''))
      console.log({track1: track1.toString()})
      crt310.writeTrack(1, track1);
    })
    .catch((error) => {
      console.log(error)
      crt310.capture();
    });
}


crt310.on('ready', () => {
  console.log('crt310 ready')
  crt310.statusRequest();
})

crt310.on('data', packet => {
  console.log('incoming', packet.command)
  if (!packet.positive) {
    if (packet.cm === 0x31) {
      // Ignore status check without card
      return
    }
    if (packet.cm == 0x37) { // write track
      crt310.capture();
    }

    // crt310.eject();
    console.log('packet not positive, halting commands');
    console.log(packet)
    //return
  }

  if (packet.cm == 0x31) {
    const STATUS = {
      NO_CARD: '00',
      GATE: '01',
      CARD: '02',
    }

    switch(packet.statusCode) {
      case STATUS.CARD:
        console.log('status card');

        break;
      case STATUS.GATE:

        console.log('status gate');
        break;
      case STATUS.NO_CARD:
        console.log('no card');
        crt310.entryBack();
        return;
    }

    setTimeout(() => {
      crt310.statusRequest();
    }, 300)
  } else if (packet.cm == 0x3a && packet.pm == 0x30) { //enable
  } else if (packet.cm == 0x3a && packet.pm == 0x31) { //disable
  } else if (packet.cm == 0x34) {
  } else if (packet.cm == 0x36 && packet.pm === 0x37) {
  } else if (packet.cm == 0x36 && packet.pm > 0x30 && packet.pm < 0x37) {
  } else if (packet.cm == 0x33 && packet.pm === 0x31) { // captured
    crt310.statusRequest();
  } else if (packet.cm == 0x33 && packet.pm === 0x30) { // Ejected
    crt310.statusRequest();
  } else if (packet.cm == 0x32 && packet.pm === 0x32) { // back entry
    askQuestions();
  } else if (packet.cm == 0x37) { // write track
    crt310.eject()
  }
})

console.log('setup complete')
