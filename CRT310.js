const util = require('util');
const EventEmitter = require('events');
const SerialPort = require('serialport')
const debug = require('debug')('CRT310');
const error = require('debug')('CRT310:error');

const CRT310Packet = require('./CRT310Packet');
const CRT310Response = require('./CRT310Response');

const HEX = 0x10


const ACK  = 0x06
const NAK  = 0x15
const DLE = 0x10
const EOT = 0x04

const EXCESSIVE_DATA_LIMIT = 1024;
const checkError = (err) => err ? error(err) : null

//const init = new CRT310Packet(Buffer.from('4330303332343030303030303030', 'hex'));
const init = new CRT310Packet(Buffer.from('C0032400001010'))
const statusRequest = new CRT310Packet(Buffer.from('C11'));

const entryBack = new CRT310Packet(Buffer.from('C22'));

const cardCarryEject = new CRT310Packet(Buffer.from('C30'));
const cardCarryCapture = new CRT310Packet(Buffer.from('C31'));

const retrieve = new CRT310Packet(Buffer.from('C40'));

const magTrackReadMove = new CRT310Packet(Buffer.from('C60'));
const magTrackRead1 = new CRT310Packet(Buffer.from('C61'));
const magTrackRead2 = new CRT310Packet(Buffer.from('C62'));
const magTrackRead3 = new CRT310Packet(Buffer.from('C63'));
const magTrackReadStatus = new CRT310Packet(Buffer.from('C67'));

// Take an optional timeout
const intake = new CRT310Packet(Buffer.from('C91'));
const withdraw = new CRT310Packet(Buffer.from('C90'));

const enable = new CRT310Packet(Buffer.from('C:0'));
const disable = new CRT310Packet(Buffer.from('C:1'));


class CRT310 extends EventEmitter {
  constructor(device) {
    super()
    this.port = new SerialPort(device, {
      parity: 'even',
    })
    this.incomplete = Buffer.alloc(0)
    this.ready = false;

    // Open errors will be emitted as an error event
    this.port.on('error', (error) => {
      this.emit('error', error);
    })

    this.port.on('close', () => {
      this.emit('close');
    })

    this.port.on('open', () => {
      debug('port open');
      this.initRetry = setInterval(() => {
        if (!this.ready) {
          debug("Sending init again");
          this.write(init.serialize())
        }
      }, 15000)
      debug("Sending init");
      this.write(init.serialize())
    })

    this.port.on('data', this.onData.bind(this))
    this.writeAsync = util.promisify(this.port.write.bind(this.port));
  }

  writePacket(packet) {
    // debug('writePacket', packet.command)
    return this.write(packet.serialize())
  }
  write(data) {
    try {
      this.port.write(data, checkError);
    } catch (err) {
      console.log('Error on write: ', err.message)
    }
  }

  // Could do this more like their diagram in the pdf
  onData(chunk) {
    // debug('Chunk:', chunk)
    if (this.incomplete.length === 0) {
      if (chunk.length === 1 && chunk[0] === NAK) {
        debug("NAK");
        return
      }
    }

    // Excessive data
    if (this.incomplete.length > EXCESSIVE_DATA_LIMIT) {
      console.log("too much data without a complete packet.  Resetting and NAK'ing")
      this.incomplete = Buffer.alloc(0)
      this.port.write(Buffer.from([NAK]), checkError)
    }

    this.incomplete = Buffer.concat([this.incomplete, chunk])
    // Slice @ 1 to omit the ACK (0x06) byte
    const packet = CRT310Response.deserialize(this.incomplete.slice(1));
    if (packet) {
      //Complete
      this.incomplete = Buffer.alloc(0)
      this.port.write(Buffer.from([ACK]), checkError)
      if (packet.positive && packet.cm == 0x30 && packet.pm == 0x30) {
        // Init response
        this.ready = true;
        clearInterval(this.initRetry);
        this.initRetry = null;
        this.emit('ready');
      } else if (this.ready) {
        this.emit('data', packet)
      } else {
        console.log('supressing data in non-ready state', packet)
      }
    }
  }

  enable() {
    this.writePacket(enable)
  }

  disable() {
    this.writePacket(disable)
  }

  retrieve() {
    this.writePacket(retrieve)
  }

  intake() {
    this.writePacket(intake)
  }

  withdraw() {
    this.writePacket(withdraw)
  }

  statusRequest() {
    this.writePacket(statusRequest)
  }

  entryBack() {
    this.writePacket(entryBack)
  }

  capture() {
    this.writePacket(cardCarryCapture)
  }

  eject() {
    this.writePacket(cardCarryEject)
  }

  magStatus() {
    this.writePacket(magTrackReadStatus)
  }

  readTrack(number) {
    switch (number) {
      case 1:
        this.writePacket(magTrackRead1)
        break;
      case 2:
        this.writePacket(magTrackRead2)
        break;
      case 3:
        this.writePacket(magTrackRead3)
        break;
    }
  }

  writeTrack(number, data) {
    const text = Buffer.alloc(3 + data.length);
    text[0] = 0x43; //'C'
    text[1] = 0x37; //'7'
    text[2] = 0x30 + number;
    data.copy(text, 3);
    //console.log('writeTrack', text.toString('hex'))
    const command = new CRT310Packet(text);
    this.writePacket(command)
  }

  clearTheLine() {
    this.port.write(Buffer.from([DLE, EOT]));
  }

  close() {
    this.port.close();
  }
}

module.exports = CRT310;
