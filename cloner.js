const util = require('util');
const SerialPort = require('serialport')

const CRT310 = require('./CRT310');
const CRT310Packet = require('./CRT310Packet');

const NOT_FOUND = -1;

const device = "/dev/cu.usbserial-APCMn19A322"

const crt310 = new CRT310(device);

crt310.on('ready', () => {
  console.log('crt310 ready')
  crt310.enable();
})

// Will store which tracks have data
const tracks = []
const trackData = [];

crt310.on('data', packet => {
  //console.log('incoming', packet.command)
  if (!packet.positive) {
    if (packet.cm === 0x31) {
      // Ignore status check without card
      return
    }

    // crt310.eject();
    console.log('packet not positive, halting commands');
    console.log(packet)
    //return
  }

  if (packet.cm == 0x31) {
    const STATUS = {
      NO_CARD: '00',
      GATE: '01',
      CARD: '02',
    }

    switch(packet.statusCode) {
      case STATUS.CARD:
        //console.log('status card');
        crt310.magStatus();
        return;
      case STATUS.GATE:
        //console.log('status gate');
        break;
      case STATUS.NO_CARD:
        //console.log('status no card');
        if (trackData.length > 0) {
          crt310.disable();
          return
        }
        break;
    }

    setTimeout(() => {
      crt310.statusRequest();
    }, 300)
  } else if (packet.cm == 0x3a && packet.pm == 0x30) { //enable
    crt310.statusRequest();
  } else if (packet.cm == 0x3a && packet.pm == 0x31) { //disable
    crt310.entryBack();
  } else if (packet.cm == 0x34) {
    crt310.magStatus();
  } else if (packet.cm == 0x36 && packet.pm === 0x37) {
    // The 0th byte is '0x60' and i don't know why
    for (var i = 1; i < packet.data.length; i++) {
      if (packet.data[i] === 0x31) {
        tracks.push(i);
      }
    }

    if (tracks.length === 0) {
      crt310.eject()
    } else {
      crt310.readTrack(tracks.shift());
    }
  } else if (packet.cm == 0x36 && packet.pm > 0x30 && packet.pm < 0x37) {
    console.log('track', String.fromCharCode(packet.pm), packet.data.toString('ascii'));
    trackData.push({
      number: packet.pm - 0x30,
      data: packet.data.slice(1),
    })

    if (tracks.length > 0) {
      crt310.readTrack(tracks.shift())
    } else {
      crt310.eject()
    }
  } else if (packet.cm == 0x33 && packet.pm === 0x30) { // Ejected
    if (trackData.length === 0) {
      crt310.enable();
    } else {
      crt310.statusRequest();
    }
  } else if (packet.cm == 0x32 && packet.pm === 0x32) { // back entry
    const track = trackData.shift();
    if (track) {
      const { number, data } = track
      crt310.writeTrack(number, data);
    } else {
      crt310.eject();
    }
  } else if (packet.cm == 0x37) { // write track
    const track = trackData.shift();
    if (track) {
      const { number, data } = track
      crt310.writeTrack(number, data);
    } else {
      crt310.eject()
    }
  }
})
